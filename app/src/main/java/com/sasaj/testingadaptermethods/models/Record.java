package com.sasaj.testingadaptermethods.models;

import java.util.Date;

/**
 * Created by DS on 10/7/2017.
 */

public class Record {
    String name;
    Date date;

    public Record(String name, Date date) {
        this.name = name;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
