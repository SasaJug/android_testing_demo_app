package com.sasaj.testingadaptermethods.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by DS on 10/6/2017.
 */

public class ViewWrapper<V extends View> extends RecyclerView.ViewHolder {

    private final V view;

    public ViewWrapper(V itemView) {
        super(itemView);
        view = itemView;
    }

    public V getView() {
        return view;
    }

}