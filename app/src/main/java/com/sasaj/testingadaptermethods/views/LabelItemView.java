package com.sasaj.testingadaptermethods.views;

import android.content.Context;
import android.widget.TextView;

import com.sasaj.testingadaptermethods.R;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by DS on 10/7/2017.
 */


@EViewGroup(R.layout.view_item_label)
public class LabelItemView extends BaseItemView {

    @ViewById
    TextView timeLabel;

    public LabelItemView(Context context) {
        super(context);
    }

    public void bind(final String text) {
        timeLabel.setText(text);
    }

}