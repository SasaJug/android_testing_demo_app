package com.sasaj.testingadaptermethods.views;

import android.content.Context;

import com.sasaj.testingadaptermethods.R;

import org.androidannotations.annotations.EViewGroup;


/**
 * Created by sjugurdzija on 14.9.17..
 */

@EViewGroup(R.layout.view_long_separator)
public class LongSeparatorItemView extends BaseItemView {

    public LongSeparatorItemView(Context context) {
        super(context);
    }
}
