package com.sasaj.testingadaptermethods.views;

import android.content.Context;
import android.widget.RelativeLayout;

import org.androidannotations.annotations.EViewGroup;

/**
 * Created by DS on 10/6/2017.
 */


@EViewGroup
public class BaseItemView extends RelativeLayout {

    public BaseItemView(Context context) {
        super(context);
    }

}
