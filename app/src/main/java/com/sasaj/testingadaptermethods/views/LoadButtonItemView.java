package com.sasaj.testingadaptermethods.views;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.sasaj.testingadaptermethods.R;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;


/**
 * Created by sjugurdzija on 11.9.17..
 */
@EViewGroup(R.layout.view_item_load_button)
public class LoadButtonItemView extends BaseItemView {

        @ViewById
        TextView button;

        public LoadButtonItemView(Context context) {
            super(context);
        }

        public void bind(final View.OnClickListener clickListener) {
            button.setOnClickListener(clickListener);
        }


}
