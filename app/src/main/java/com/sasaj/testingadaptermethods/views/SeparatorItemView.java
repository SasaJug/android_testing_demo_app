package com.sasaj.testingadaptermethods.views;

import android.content.Context;

import com.sasaj.testingadaptermethods.R;

import org.androidannotations.annotations.EViewGroup;


/**
 * Created by sjugurdzija on 11.9.17.
 */

@EViewGroup(R.layout.view_item_separator)
public class SeparatorItemView extends BaseItemView {

    public SeparatorItemView(Context context) {
        super(context);
    }
}