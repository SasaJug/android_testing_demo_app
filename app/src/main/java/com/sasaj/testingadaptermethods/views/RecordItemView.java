package com.sasaj.testingadaptermethods.views;

import android.content.Context;
import android.widget.TextView;

import com.sasaj.testingadaptermethods.R;
import com.sasaj.testingadaptermethods.models.Record;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;

/**
 * Created by DS on 10/7/2017.
 */

@EViewGroup(R.layout.view_item_record)
public class RecordItemView extends BaseItemView {

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd. MM. yyyy / HH:mm");

    @ViewById
    public TextView recordName;

    @ViewById
    public TextView recordDate;

    public RecordItemView(Context context) {
        super(context);
    }

    public void bind(final Record record) {

        recordName.setText(record.getName());
        if (record.getDate() != null) {
            String date = simpleDateFormat.format(record.getDate());
            recordDate.setText("Submitted on " + date + "h");
        }

    }
}

