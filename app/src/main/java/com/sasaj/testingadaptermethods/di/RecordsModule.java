package com.sasaj.testingadaptermethods.di;

import android.app.usage.UsageEvents;
import android.content.Context;

import com.sasaj.testingadaptermethods.MainContract;
import com.sasaj.testingadaptermethods.MainPresenter;
import com.sasaj.testingadaptermethods.RecordsRepository;
import com.sasaj.testingadaptermethods.RecordsRepositoryImplementation;
import com.sasaj.testingadaptermethods.adapter.SampleAdapter;
import com.sasaj.testingadaptermethods.eventbus.EventBus;
import com.sasaj.testingadaptermethods.eventbus.OttoBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by DS on 10/14/2017.
 */

@Module
public class RecordsModule {

    private final MainContract.View view;

    public RecordsModule(MainContract.View view) {
        this.view = view;
    }

    @Provides
    @Singleton
    SampleAdapter providesAdapter(){
        return new SampleAdapter();
    }

    @Provides
    @Singleton
    MainContract.UserActionsListener providesPresenter(MainContract.View view, EventBus bus, RecordsRepository repository){
        return new MainPresenter(view, bus, repository);
    }


    @Provides
    @Singleton
    MainContract.View providesRecordsView(){
        return this.view;
    }

    @Provides
    @Singleton
    RecordsRepository providesRecordsRepository(EventBus bus){
        return new RecordsRepositoryImplementation(bus);
    }

    @Provides
    @Singleton
    EventBus providesEventBus(){
        return OttoBus.getInstance();
    }

}
