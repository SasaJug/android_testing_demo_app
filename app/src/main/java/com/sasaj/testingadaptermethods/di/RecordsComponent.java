package com.sasaj.testingadaptermethods.di;

import com.sasaj.testingadaptermethods.MainActivity;
import com.sasaj.testingadaptermethods.MainContract;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by DS on 10/14/2017.
 */

@Singleton
@Component(modules = {RecordsModule.class})
public interface RecordsComponent {

    void inject(MainActivity activity);

}
