package com.sasaj.testingadaptermethods;

import android.app.Application;
import android.os.Handler;

import com.sasaj.testingadaptermethods.di.DaggerRecordsComponent;
import com.sasaj.testingadaptermethods.di.RecordsComponent;
import com.sasaj.testingadaptermethods.di.RecordsModule;

/**
 * Created by DS on 10/14/2017.
 */

public class RecordsApp extends Application {

    public static Handler uiHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        uiHandler = new Handler(getMainLooper());

    }


    public RecordsComponent getRecordsComponent(MainContract.View view){
        return DaggerRecordsComponent
                .builder()
                .recordsModule(new RecordsModule(view))
                .build();
    }
}
