package com.sasaj.testingadaptermethods.adapter;

import com.sasaj.testingadaptermethods.models.Record;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DS on 10/7/2017.
 */

public class GenericAdapterHelper {

    public static final int RECORD = 0;
    public static final int LABEL = 1;
    public static final int BUTTON = 2;
    public static final int SEPARATOR = 3;
    public static final int LONG_SEPARATOR = 4;

    private static final int STEP = 5;
    private List<Record> records = new ArrayList<>();
    private int maxVisibleElements;

    public GenericAdapterHelper() {
        maxVisibleElements = STEP;
    }

    public List<Object> getListItems() {
        List<Object> finalList = new ArrayList<>();

        if(this.records == null){
            throw new IllegalStateException("Records list must be set before calling getListItems");
        }
        if (records.size() <= 0) {
            // Do not show any elements in "Submitted list" section if list is empty.
        } else {
            if (records.size() >= maxVisibleElements) {
                for (int i = 0; i < maxVisibleElements; i++) {
                    finalList.add(records.get(i));
                    //i < records.size() - 1  -  Decreased to 1 after testing
                    if (i < records.size() - 1 && i < maxVisibleElements - 1) {
                        finalList.add(SEPARATOR);
                    }
                }
                if (records.size() > maxVisibleElements) {
                    finalList.add(BUTTON);
                }
            } else {
                for (int i = 0; i < records.size(); i++) {
                    finalList.add(records.get(i));
                    if (i < records.size() - 1) {
                        finalList.add(SEPARATOR);
                    }
                }
            }

            finalList.add(0, LABEL);
            finalList.add(LONG_SEPARATOR);
        }
        return finalList;
    }

    public int getItemType(Object item) {
        if (item instanceof Integer) {
            return (int) item;
        } else {
            return RECORD;
        }
    }

    public void setRecords(List<Record> records){
        this.records = records;
        resetMaxVisibleItems();
    }

    public void increaseMaxVisibleItems() {
        maxVisibleElements += STEP;
    }

    public void resetMaxVisibleItems() {
        maxVisibleElements = STEP;
    }
}
