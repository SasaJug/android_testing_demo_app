package com.sasaj.testingadaptermethods.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.sasaj.testingadaptermethods.models.Record;
import com.sasaj.testingadaptermethods.views.BaseItemView;
import com.sasaj.testingadaptermethods.views.LabelItemView;
import com.sasaj.testingadaptermethods.views.LabelItemView_;
import com.sasaj.testingadaptermethods.views.LoadButtonItemView;
import com.sasaj.testingadaptermethods.views.LoadButtonItemView_;
import com.sasaj.testingadaptermethods.views.LongSeparatorItemView_;
import com.sasaj.testingadaptermethods.views.RecordItemView;
import com.sasaj.testingadaptermethods.views.RecordItemView_;
import com.sasaj.testingadaptermethods.views.SeparatorItemView_;
import com.sasaj.testingadaptermethods.views.ViewWrapper;
import java.util.List;
import static com.sasaj.testingadaptermethods.adapter.GenericAdapterHelper.*;


public class SampleAdapter extends RecyclerViewAdapterBase<Object, BaseItemView> {

    protected final static String TAG = SampleAdapter.class.getSimpleName();


    private GenericAdapterHelper helper;

    public SampleAdapter() {
        super();
        this.helper = new GenericAdapterHelper();
    }

    @Override
    protected BaseItemView onCreateItemView(ViewGroup parent, int viewType) {
        switch (viewType) {
            case RECORD:
                return RecordItemView_.build(parent.getContext());
            case LABEL:
                return LabelItemView_.build(parent.getContext());
            case BUTTON:
                return LoadButtonItemView_.build(parent.getContext());
            case SEPARATOR:
                return SeparatorItemView_.build(parent.getContext());
            case LONG_SEPARATOR:
                return LongSeparatorItemView_.build(parent.getContext());
            default:
                throw new IllegalArgumentException("View type not supported.");
        }
    }

    @Override
    public void onBindViewHolder(final ViewWrapper<BaseItemView> holder, int position) {

        BaseItemView view = holder.getView();

        if (view instanceof RecordItemView) {
            final Record record = (Record) items.get(position);
            final RecordItemView recordItemView = (RecordItemView) view;
            recordItemView.bind(record);

        } else if (view instanceof LabelItemView) {
            LabelItemView labelItemView = (LabelItemView) view;
            final String text = "Submitted Records";
            labelItemView.bind(text);

        } else if (view instanceof LoadButtonItemView) {
            LoadButtonItemView buttonView = (LoadButtonItemView) view;
            buttonView.bind(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    increaseMaxVisibleElements();
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return helper.getItemType(items.get(position));
    }


    private void createListItems() {
        this.items = helper.getListItems();
        notifyDataSetChanged();
    }

    public void setRecords(List<Record> records) {
        helper.setRecords(records);
        createListItems();
    }

    private void increaseMaxVisibleElements() {
        helper.increaseMaxVisibleItems();
        createListItems();
    }

}

