package com.sasaj.testingadaptermethods.event;

import com.sasaj.testingadaptermethods.models.Record;

import java.util.List;

/**
 * Created by DS on 10/14/2017.
 */

public class RecordEvent {
    List<Record> records;
    private String error;

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
