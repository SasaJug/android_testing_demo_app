package com.sasaj.testingadaptermethods;

import android.util.Log;

import com.sasaj.testingadaptermethods.event.RecordEvent;
import com.sasaj.testingadaptermethods.eventbus.EventBus;
import com.squareup.otto.Subscribe;

/**
 * Created by DS on 10/14/2017.
 */

public class MainPresenter implements MainContract.UserActionsListener {


    private MainContract.View view;
    private EventBus eventBus;
    private RecordsRepository repository;

    public MainPresenter(MainContract.View view, EventBus eventBus, RecordsRepository repository) {
        this.view = view;
        this.eventBus = eventBus;
        this.repository = repository;
    }


    @Override
    public void onResume() {
        eventBus.register(this);
    }

    @Override
    public void onPause() {
        eventBus.unregister(this);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void getRecords() {
        view.showProgress();
        repository.getRecords();
    }

    @Override
    @Subscribe
    public void onEventMainThread(RecordEvent event) {

        String errorMsg = event.getError();
        if (view != null) {
            view.hideProgress();
            if (errorMsg != null) {
                view.onError(errorMsg);
            } else {
                view.setRecords(event.getRecords());
            }
        }
    }
}


