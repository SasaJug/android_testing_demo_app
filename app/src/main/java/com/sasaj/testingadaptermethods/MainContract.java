package com.sasaj.testingadaptermethods;

import com.sasaj.testingadaptermethods.event.RecordEvent;
import com.sasaj.testingadaptermethods.models.Record;

import java.util.List;

/**
 * Created by DS on 10/14/2017.
 */

public interface MainContract {

    interface View {
        void showProgress();

        void hideProgress();

        void setRecords(List<Record> records);

        void onError(String msg);
    }

    interface UserActionsListener {
        void onResume();
        void onPause();
        void onDestroy();
        void getRecords();
        void onEventMainThread(RecordEvent event);
    }
}
