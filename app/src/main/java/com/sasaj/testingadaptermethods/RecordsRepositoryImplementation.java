package com.sasaj.testingadaptermethods;

import com.sasaj.testingadaptermethods.event.RecordEvent;
import com.sasaj.testingadaptermethods.eventbus.EventBus;
import com.sasaj.testingadaptermethods.eventbus.OttoBus;
import com.sasaj.testingadaptermethods.models.Record;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by DS on 10/14/2017.
 */

public class RecordsRepositoryImplementation implements RecordsRepository {

    private final EventBus eventBus;

    public RecordsRepositoryImplementation(EventBus bus) {
        this.eventBus = bus;
    }

    @Override
    public void getRecords() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                List<Record> records = new ArrayList<>();

                records.add(new Record("First", new Date()));
                records.add(new Record("Second", new Date()));
                records.add(new Record("Third", new Date()));
                records.add(new Record("Fourth", new Date()));
                records.add(new Record("Fifth", new Date()));
                records.add(new Record("Sixth", new Date()));
                records.add(new Record("Seventh", new Date()));
                records.add(new Record("Eighth", new Date()));
                records.add(new Record("Ninth", new Date()));
                records.add(new Record("Tenth", new Date()));
                records.add(new Record("Eleventh", new Date()));
                records.add(new Record("Twelfth", new Date()));

                final RecordEvent event = new RecordEvent();
                event.setRecords(records);
                RecordsApp.uiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        eventBus.post(event);
                    }
                });
            }
        }).start();

    }
}
