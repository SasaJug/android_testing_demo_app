package com.sasaj.testingadaptermethods;

import com.sasaj.testingadaptermethods.models.Record;

import java.util.List;

/**
 * Created by DS on 10/14/2017.
 */

public interface RecordsRepository {
    void getRecords();
}
