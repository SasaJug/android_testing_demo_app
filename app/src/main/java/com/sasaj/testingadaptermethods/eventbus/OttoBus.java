package com.sasaj.testingadaptermethods.eventbus;

import com.squareup.otto.Bus;

/**
 * Created by DS on 10/14/2017.
 */

public class OttoBus implements EventBus {

    private static OttoBus instance = null;
    private final Bus eventBus;

    public static synchronized OttoBus getInstance(){
        if(instance == null)
            instance = new OttoBus();
        return instance;
    }

    private OttoBus() {
        this.eventBus = new Bus();
    }


    @Override
    public void register(Object subscriber) {
        eventBus.register(subscriber);
    }

    @Override
    public void unregister(Object subscriber) {
        eventBus.unregister(subscriber);
    }

    @Override
    public void post(Object event) {
        eventBus.post(event);
    }
}

