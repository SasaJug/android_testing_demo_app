package com.sasaj.testingadaptermethods.eventbus;

/**
 * Created by DS on 10/14/2017.
 */

public interface EventBus {
    void register(Object subscriber);
    void unregister(Object subscriber);
    void post(Object event);
}
