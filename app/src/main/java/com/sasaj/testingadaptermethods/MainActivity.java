package com.sasaj.testingadaptermethods;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.sasaj.testingadaptermethods.adapter.SampleAdapter;
import com.sasaj.testingadaptermethods.di.RecordsComponent;
import com.sasaj.testingadaptermethods.eventbus.OttoBus;
import com.sasaj.testingadaptermethods.models.Record;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    ProgressBar progress;
    RecyclerView list;

    @Inject
    SampleAdapter adapter;

    @Inject
    MainContract.UserActionsListener presenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupInjection();
        list = (RecyclerView) findViewById(R.id.recordsRecyclerView);
        list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        progress = (ProgressBar) findViewById(R.id.progress);

        list.setAdapter(adapter);
    }

    private void setupInjection() {
        RecordsApp app = (RecordsApp) getApplication();
        RecordsComponent recordsComponent = app.getRecordsComponent(this);
        //presenter = imagesComponent.getPresenter();
        recordsComponent.inject(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
        presenter.getRecords();
    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void setRecords(final List<Record> records) {
        adapter.setRecords(records);
    }

    @Override
    public void onError(String msg) {

    }
}
