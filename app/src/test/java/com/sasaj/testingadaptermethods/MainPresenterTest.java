package com.sasaj.testingadaptermethods;

import com.sasaj.testingadaptermethods.event.RecordEvent;
import com.sasaj.testingadaptermethods.eventbus.EventBus;
import com.sasaj.testingadaptermethods.models.Record;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by DS on 10/16/2017.
 */
public class MainPresenterTest {

    private List<Record> list1 = new ArrayList<>();
    private List<Record> list2 = new ArrayList<>();
    private MainContract.View view;
    private EventBus eventBus;
    private RecordsRepository repository;
    private RecordEvent event;
    private MainPresenter presenter;



    @Before
    public void setUp() throws Exception {
        view = Mockito.mock(MainContract.View.class);
        eventBus = Mockito.mock(EventBus.class);
        repository = Mockito.mock(RecordsRepository.class);
        event = Mockito.mock(RecordEvent.class);
        presenter = new MainPresenter(view, eventBus, repository);

        list1.add(new Record("Test Record 1", new Date()));
        list1.add(new Record("Test Record 2", new Date()));
        list1.add(new Record("Test Record 3", new Date()));


        list2.add(new Record("Test Record 4", new Date()));
        list2.add(new Record("Test Record 5", new Date()));
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void noRecords() throws Exception {
        Mockito.when(event.getRecords()).thenReturn(null);
        presenter.onEventMainThread(event);
        Mockito.verify(view, Mockito.times(1)).hideProgress();
        Mockito.verify(view, Mockito.times(1)).setRecords(null);
    }

    @Test
    public void listWithRecords() throws Exception {
        Mockito.when(event.getRecords()).thenReturn(list1);
        presenter.onEventMainThread(event);
        Mockito.verify(view, Mockito.times(1)).hideProgress();
        Mockito.verify(view, Mockito.times(1)).setRecords(list1);
    }


}