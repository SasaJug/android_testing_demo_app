package com.sasaj.testingadaptermethods.adapter;

import com.sasaj.testingadaptermethods.models.Record;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by DS on 10/14/2017.
 */
public class GenericAdapterHelperTest {

    private static final String list4 = "1,RECORD,3,RECORD,3,RECORD,3,RECORD,4";
    private static final String list5= "1,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,4";
    private static final String list6Start= "1,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,2,4";
    private static final String list6End= "1,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,4";

    private static final String list10Start= "1,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,2,4";
    private static final String list10End= "1,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,4";

    private static final String list11Start= "1,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,2,4";
    private static final String list11Intermediate = "1,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,2,4";
    private static final String list11End= "1,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,3,RECORD,4";

    private GenericAdapterHelper helper;
    private List<Object> items;

    @Before
    public void setUp() throws Exception {
        helper = new GenericAdapterHelper();
    }

    @Test(expected = IllegalStateException.class)
    public void getTestItemsForNull(){
        helper.setRecords(null);
        items = helper.getListItems();
    }

    @Test
    public void getTestItemsForEmpty(){
        helper.setRecords(new ArrayList<Record>());
        items = helper.getListItems();
        assertEquals("Failed on test with empty list",printItemList(items),"");
    }

    @Test
    public void getListItems() throws Exception {
        helper.setRecords(getTestRecords(4));
        items = helper.getListItems();
        assertEquals("Failed on test with 4 records",printItemList(items),list4);

        helper.setRecords(getTestRecords(5));
        items = helper.getListItems();
        assertEquals("Failed on test with 5 records",printItemList(items),list5);

        helper.setRecords(getTestRecords(6));
        items = helper.getListItems();
        assertEquals("Failed on test with 6 records before button is pressed",printItemList(items),list6Start);
        helper.increaseMaxVisibleItems();
        items = helper.getListItems();
        assertEquals("Failed on test with 6 records after button is pressed", printItemList(items),list6End);

        helper.setRecords(getTestRecords(10));
        items = helper.getListItems();
        assertEquals("Failed on test with 10 records before button is pressed",printItemList(items),list10Start);
        helper.increaseMaxVisibleItems();
        items = helper.getListItems();
        assertEquals("Failed on test with 10 records after button is pressed", printItemList(items),list10End);

        helper.setRecords(getTestRecords(11));
        items = helper.getListItems();
        assertEquals("Failed on test with 11 records before button is pressed first time",printItemList(items),list11Start);
        helper.increaseMaxVisibleItems();
        items = helper.getListItems();
        assertEquals("Failed on test with 11 records after button is pressed first time", printItemList(items), list11Intermediate);
        helper.increaseMaxVisibleItems();
        items = helper.getListItems();
        assertEquals("Failed on test with 11 records after button is pressed second time", printItemList(items),list11End);
    }

    @After
    public void tearDown() throws Exception {
        helper = null;
    }


    private List<Record> getTestRecords(int quantity) {
        List<Record> testRecords = new ArrayList<>();
        for(int i = 0; i < quantity; i ++){
            testRecords.add(new Record("Record number" + i, new Date()));
        }

        return testRecords;
    }

    private String printItemList(List<Object> items){
        StringBuilder builder = new StringBuilder();
        for(Object item : items){
            if(item instanceof Record){
                builder.append("RECORD");
            } else {
                builder.append(item);
            }
            if(items.indexOf(item)<items.size()-1){
                builder.append(",");
            }
        }

        return builder.toString();
    }

}