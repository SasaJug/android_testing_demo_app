# README #

This Android project demonstrates various aspects of unit testing and dependency injection using Dagger 2 dependency injection framework.

### Sources ###

* Engineering Maintainable Android Apps (Coursera)
* Effective Android Testing for Mobile Developers (LinkedIn Learning)
* Android App Development: Unit Testing (LinkedIn Learning)
